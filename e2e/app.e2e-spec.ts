import {AdminPage} from './app.po';
import {browser, by, element} from 'protractor';

describe('Admin panel add buttons', () => {
  let page: AdminPage;
  const fields = new Map<string, string[]>();
  fields['Name'] = ['tags', 'personGroups', 'zones', 'doors'];
  fields['Zones'] = ['personGroups', 'doors'];
  fields['Active'] = ['tags', 'idCards', 'persons'];
  fields['Document'] = ['idCards'];
  fields['First name'] = ['persons'];
  fields['Last name'] = ['persons'];
  fields['Id code'] = ['persons'];
  fields['Person groups'] = ['zones', 'persons'];
  fields['Doors'] = ['zones'];
  fields['Uid'] = ['tags'];
  fields['Persons'] = ['personGroups'];
  fields['Person'] = ['tags', 'idCards'];

  const menu = ['personGroups', 'tags', 'zones', 'doors', 'persons', 'idCards', 'doorLog'];


  beforeEach(() => {

  });


  afterEach(() => {
    page.closeDialog();
  });

  it('i can edit uid/name/active', () => {
    page = new AdminPage();
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('No records found');
    page.clickAddButton();
    page.saveFields();
  });

  // it('should be able Add items in all views', async () => {
  //   for (const menuItem of  menu){
  //     const item = menu.indexOf(menuItem);
  //     page.tableAddTest(menuItem);
  //     element(by.xpath('/html/body/app-root/app-ngrx-datatable/div/p-dialog/div/div[1]/a/span')).click();
  //   }
  // });

  it('should be able add personGroups', async () => {
    const menuItem = 'personGroups';
    page.tableAddTest(menuItem);
    browser.takeScreenshot();
  });

  it('should be able add tags', async () => {
    const menuItem = 'tags';
    page.tableAddTest(menuItem);
  });

  it('should be able add zones', async () => {
    const menuItem = 'zones';
    page.tableAddTest(menuItem);
  });

  it('should be able add doors', async () => {
    const menuItem = 'doors';
    page.tableAddTest(menuItem);
  });

  it('should be able add persons', async () => {
    const menuItem = 'persons';
    page.tableAddTest(menuItem);
  });

  it('should be able add idCards', async () => {
    const menuItem = 'idCards';
    page.tableAddTest(menuItem);
  });


  //
  // it('should be able not add doorLog', async () => {
  //   const menuItem = 'doorLog';
  //   page.tableNotAddTest(menuItem);
  // });

});
