import {AdminPage} from './app.po';
import {browser, by, element} from 'protractor';

describe('Admin panel login', () => {
  let page: AdminPage;


  beforeEach(() => {
    page = new AdminPage();
    page.navigateTo('/#/login');
  });


  afterEach(() => {

  });

  it('i can login', async () => {
    element(by.xpath('//*[@id="userName"]')).sendKeys('admin');
    element(by.xpath('//*[@id="pswd"]')).sendKeys('admin');
    element(by.xpath('/html/body/app-root/app-login/div/div/form/button')).click();
    page.clickOnMenuButton('idCards');
  });


  // it('should be able Add items in all views', async () => {
  //   for (const menuItem of  menu){
  //     const item = menu.indexOf(menuItem);
  //     page.tableAddTest(item, menuItem, fields);
  //     element(by.xpath('/html/body/app-root/app-ngrx-datatable/div/p-dialog/div/div[1]/a/span')).click();
  //   }
  // });


});
