import {AdminPage} from './app.po';
import {browser, by, element} from 'protractor';

describe('Admin panel fill person', () => {
  let page: AdminPage;


  beforeEach(() => {
    page = new AdminPage();
    page.navigateTo();
  });


  afterEach(() => {

  });

  it('i can add uid/name/active', async () => {
    page.clickOnMenuButton('idCards');
    page.clickAddButton();
    element(by.xpath('//*[@id="active"]')).click();
    element(by.xpath('//*[@id="document"]')).sendKeys('AA124124');
    element(by.xpath('//*[@id="person"]/span/input')).sendKeys('H');
    element(by.xpath('//*[@id="person"]/span/div/ul/li')).click();
    page.clickSaveButton();

  });


  it('i can edit uid/name/active', async () => {
    page.clickOnMenuButton('idCards');
    page.clickEditButton(1);
    element(by.xpath('//*[@id="person"]/span/input')).sendKeys('H');
    element(by.xpath('//*[@id="person"]/span/div/ul/li')).click();
    page.clickSaveButton();

  });

  // it('should be able Add items in all views', async () => {
  //   for (const menuItem of  menu){
  //     const item = menu.indexOf(menuItem);
  //     page.tableAddTest(item, menuItem, fields);
  //     element(by.xpath('/html/body/app-root/app-ngrx-datatable/div/p-dialog/div/div[1]/a/span')).click();
  //   }
  // });


});
