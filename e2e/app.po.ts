import {browser, by, element} from 'protractor';
import {until} from 'selenium-webdriver';
import elementsLocated = until.elementsLocated;

export class AdminPage {
  private fields: Map<string, string[]> = new Map<string, string[]>();


  private menu = [
    'zones',
    'doorLog',
    'tags',
    'personGroups',
    'doors',
    'persons',
    'idCards',
  ];

  saveFields() {
    this.fields['Name'] = ['tags', 'personGroups', 'zones', 'doors'];
    this.fields['Zones'] = ['personGroups', 'doors'];
    this.fields['Active'] = ['tags', 'idCards', 'persons'];
    this.fields['Document'] = ['idCards'];
    this.fields['First name'] = ['persons'];
    this.fields['Last name'] = ['persons'];
    this.fields['Id code'] = ['persons'];
    this.fields['Person groups'] = ['zones', 'persons'];
    this.fields['Doors'] = ['zones'];
    this.fields['Uid'] = ['tags'];
    this.fields['Persons'] = ['personGroups'];
    this.fields['Person'] = ['tags', 'idCards'];
  }

  navigateTo(url?: string) {
    url = url || '/';
    return browser.get(url);

  }

  getParagraphText() {
    return element(
      by.xpath('//p-datatable/div/div[1]/table/tbody/tr/td')).getText();
  }


  tableAddTest(active: string) {
    this.clickOnMenuButton(active);
    this.clickAddButton();
    const res = this.getDialogContent();
    for (const row in this.fields) {
      if (this.fields[row].includes(active)) {
        expect(res).toContain(row);
      }
    }
  }

  tableNotAddTest(active: string) {
    this.clickOnMenuButton(active);
    this.clickAddButton();
    const res = this.isDialogContentDisplayed();
    expect(res).toBeFalsy();
  }


  clickAddButton() {
    element(by.xpath('//app-datatable/div[1]/button/span[2]')).click();
  }


  closeDialog() {
    element(by.xpath('//p-dialog/div/div[1]/a/span')).click();
  }

  clickOnMenuButton(active: string) {


    const item = this.menu.indexOf(active) + 1;
    element(
      by.xpath('//p-selectbutton//*[contains(text(),\'' + active + '\')]')
    ).click();
  }


  getDialogContent() {
    return element(by.xpath('//p-dialog/div/div[2]')).getText();
  }

  isDialogContentDisplayed() {
    return element(by.xpath('//p-dialog')).isDisplayed();
  }


  clickSaveButton() {
    //noinspection TsLint
    element(by.xpath('//p-dialog//*[contains(text(), \'Save\')]')).click();
  }

  clickEditButton(row: number) {
    element.all(by.className('fa-edit')).first().click();
  }
}
