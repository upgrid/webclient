// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const {SpecReporter} = require('jasmine-spec-reporter');

exports.config = {
  allScriptsTimeout: 11000,
  specs: [
    './e2e/**/login.e2e-spec.ts'
    //'./e2e/**/*.e2e-spec.ts'
  ],

  capabilities: {
    'browserName': 'chrome',
    'maxInstances': 5,
  },
  directConnect: true,
  baseUrl: 'http://localhost:4200/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    defaultTimeoutInterval: 30000,
    onComplete: null,
    isVerbose: false,
    showColors: true,
    includeStackTrace: true,
    singleRun: true,
    print: function () {
    }
  },
  plugins: [{
    package: 'protractor-screenshoter-plugin',
    screenshotPath: './REPORTS/e2e',
    screenshotOnExpect: 'failure+success',
    screenshotOnSpec: 'none',
    withLogs: 'true',
    writeReportFreq: 'asap',
    imageToAscii: '',
    clearFoldersBeforeTest: true
  }],
  beforeLaunch: function () {
    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
  },
  onPrepare() {
    jasmine.getEnv().addReporter(new SpecReporter({spec: {displayStacktrace: true}}));
    return global.browser.getProcessedConfig().then(function (config) {
      //it is ok to be empty
    });
  },

};
