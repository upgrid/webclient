import {Component, NgZone, OnInit} from '@angular/core';
import {LoginService} from '../services/login.service';
import {Router} from '@angular/router';
import {AppComponent} from '../app.component';
import {AppRoutingModule} from '../app-routing.module';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userName: string;
  passWord: string;
  failLogin = false;
  sent = false;
  errorMessage = '';
  private bota: string;

  constructor(private loginService: LoginService, private _router: Router, private zone: NgZone) {
  }

  ngOnInit() {
  }

  onSubmit() {
    this.onLogin();

  }

  onLogin() {
    this.bota = btoa(this.userName + ':' + this.passWord);
    if (!this.sent) {
      return this.loginService.getToken(this.bota).subscribe(
        data => this.setToken(data),
        error => this.errorMessage = 'Wrong username or password'
      );
    }
    this.sent = true;
  }

  setToken(data: any) {
    this.errorMessage = '';
    AppComponent.userToken = this.bota;
    AppComponent.rootLinks = data;
    console.log(AppComponent.userToken);
    this.afterLogin()
    //AppComponent.userToken = data['token'];
    //StorageHelper.saveAdminTokenToLS(AppComponent.userToken);
  }

  afterLogin(error?) {
    if (error && this.sent) {
      console.log('error: ' + JSON.stringify(error));
      // this.errorMessage = "Teie Google kontol ei ole administraatori õiguseid!";
      this.errorMessage = error._body;
    }
    else console.log('After login: ' + AppComponent.userToken);
    this.sent = false;

    this.zone.run(() => this._router.navigate(['/']));
  }

}
