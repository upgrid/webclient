import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginService} from './services/login.service';
import {RepositoryService} from './services/repository.service';
import {HalModule} from 'ng-hal';
import {DatatableComponent} from './datatable/datatable.component';
import {
  AutoCompleteModule,
  CheckboxModule,
  CodeHighlighterModule, ContextMenuModule, DataGridModule, DataTableModule, DialogModule, GrowlModule,
  PanelModule, SelectButtonModule,
  TabMenuModule,
  TabViewModule
} from 'primeng/primeng';
import {InputTextModule, ButtonModule, ConfirmDialogModule} from 'primeng/primeng';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LoginComponent} from './login/login.component';





@NgModule({
  declarations: [
    AppComponent,
    DatatableComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    HalModule.forRoot(),
    AppRoutingModule,
    // Primeng modules
    InputTextModule,
    ButtonModule,
    ConfirmDialogModule,
    DataGridModule,
    DataTableModule,
    PanelModule,
    DialogModule,
    TabViewModule,
    CodeHighlighterModule,
    ContextMenuModule,
    CheckboxModule,
    TabMenuModule,
    AutoCompleteModule,
    GrowlModule,
    SelectButtonModule
  ],
  providers: [LoginService, RepositoryService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
