///<reference path="../models/resources.ts"/>
import {Component, OnInit} from '@angular/core';
import {RepositoryService} from '../services/repository.service';
import {MenuItem} from 'primeng/components/common/api';
import {ActivatedRoute, Router} from '@angular/router';
import {IdCard, Person, PersonGroup, Tag} from '../models/resources';
import {ConfirmationService, Message} from 'primeng/primeng';
import {AppComponent} from '../app.component';
import {LoginService} from 'app/services/login.service';


@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.css'],
  providers: [ConfirmationService],
})

export class DatatableComponent implements OnInit {
  bota: any;
  searchResults: any = [];
  personGroups: any;
  displayDialog: boolean;
  contextmenuRow: any;
  selfUrl: string;
  activeEditRow: any;

  editAble: any[];
  selectedItem: any;

  contextMenuItems: MenuItem[];

  schema: any;
  rootLinks: any[];
  itemLinksText: any[];
  rawEvent: any;
  persons: any;
  rows: any[] = [];
  columns = [];
  activeTable: any;
  selected: any;
  selectedLink: string;
  selectedSubItem: string;
  schemaArray: any[];
  private section: string;
  private sub: any;
  activeEdit: any;
  private isNewItem: boolean;
  msgs: Message[] = [];
  menuText: Map<string, string> = new Map<string, string>();
  blackListSchema: string[] = ['createDate', 'lastModified'];

  constructor(private RepositoryService: RepositoryService,
              private _route: ActivatedRoute,
              private _router: Router,
              private confirmationService: ConfirmationService,
              private loginService: LoginService,) {
    this.selfUrl = '';
    this.bota = btoa('admin:admin');
    this.loginService.getToken(this.bota).subscribe(
      data => this.setToken(data)
    );

    this.menuText['zones'] = 'Zones';
    this.menuText['persons'] = 'Persons';
    this.menuText['doorLogs'] = 'Door logs';
    this.menuText['doors'] = 'Doors';
    this.menuText['idCards'] = 'ID cards';
    this.menuText['tags'] = 'Tags';
    this.menuText['personGroups'] = 'Person groups';

  }


  setToken(data: any) {
    AppComponent.userToken = this.bota;
    AppComponent.rootLinks = data;
    console.log(AppComponent.rootLinks);
    //AppComponent.userToken = data['token'];
    //StorageHelper.saveAdminTokenToLS(AppComponent.userToken);
  }


  confirmDelete(item: string) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation',
      icon: 'fa fa-trash',
      accept: () => {
        this.msgs = [];
        this.msgs.push({severity: 'info', summary: 'Confirmed', detail: 'Record deleted'});
        this.deleteItem(item);
        this.onDelete();
      }
    });
  }

  getObj(item: string) {
    let obj: any = {};
    switch (item) {
      case 'tags': {
        obj = new Tag();
        break;
      }
      case 'persongroups': {
        obj = new PersonGroup();
        break;
      }
      case 'idcards': {
        obj = new IdCard;
        break;
      }
      case 'doors': {
        obj = new Person();
        break;
      }
      case 'persons': {
        obj = new Person();
        break;
      }
      default: {
        break;
      }
    }
    return obj;
  }

  ngOnInit() {
    this.getRoot();
    this.contextMenuItems = [
      // {label: 'View', icon: 'fa-search', command: (event) => this.viewCar(this.selectedItem)},
      {label: 'Delete', icon: 'fa-trash', command: (event) => this.deleteItem(this.selectedItem._links.self.href)}
    ];
  }

  isSection() {
    this.sub = this._route.params.subscribe(params => {
      if (params['section']) {
        this.section = params['section'];
      } else {
        this._router.navigate(['/admin/', 'persons']);
        this.section = 'persons';
      }
    });
  }

  handleMenuChange(e) {
    console.log(e);
  }


  getAllItems(item: any) {
    this.activeTable = item.value;
    this.RepositoryService.getItem(this.activeTable).subscribe(
      data => this.storePersons(data.data, data.links),
      error => this.handleError(error),
    );
  }

  storePersons(data, links) {
    this.selected = undefined;
    this.columns = [];
    this.rows = data || [];
    this.rows = data || [];
    for (const key in data[0]) {
      if (data[0].hasOwnProperty(key)) {
        this.columns.push(key);
      }
    }
  }


  onNavButtonb(event) {
    const item = event;
    const name = item.name;
    this.activeTable = this.findMatchRoot(name);
    this.getSchema();
    this.selfUrl = item.href;
    this.rows = [];
    this.RepositoryService.getUrl(item.href).subscribe(
      data => this.storePersons(data, data.links),
      error => this.handleError(error),
    );
  }

  onContextMenu(contextMenuEvent) {

    this.rawEvent = contextMenuEvent.event;
    this.contextmenuRow = contextMenuEvent.row;

    contextMenuEvent.event.preventDefault();
    contextMenuEvent.event.stopPropagation();
  }

  onSelect(selected) {
    this.selected = selected;
    console.log('Select Event', selected, this.selected);
  }

  onActivate(event) {
    console.log('Activate Event', event);
  }

  updateRowPosition() {
    const ix = this.getSelectedIx();
    const arr = [...this.rows];
    arr[ix - 1] = this.rows[ix];
    arr[ix] = this.rows[ix - 1];
    this.rows = arr;
  }

  getSelectedIx() {
    return this.selected[0]['$$index'];
  }

  getRoot() {
    AppComponent.rootLinks._linksArray = RepositoryService.remapLinks(AppComponent.rootLinks._links);
    this.rootLinks = AppComponent.rootLinks._linksArray.filter(link => {
      return link.name !== 'profile';
    }).map(link => {
      if (this.menuText[link.name] !== '') {
        return {label: this.menuText[link.name], value: link.name};
      }
    });
  }

  getSchema() {
    this.RepositoryService.getSchema(this.activeTable).subscribe(
      data => this.saveSchema(data),
      error => this.handleError(error)
    );
  }

  saveSchema(data) {
    this.schema = data;
    this.schemaArray = [];


    this.itemLinksText = [];
    for (const key in data.properties) {
      if (data.properties.hasOwnProperty(key)) {
        const prop = data.properties[key];
        if (prop.hasOwnProperty('format') && prop.format === 'uri') {
          this.itemLinksText.push({
            name: prop.title + 'FGWFWE',
            value: key
          });
        }
        prop.name = key;
        if (!this.blackListSchema.includes(key)) {
          this.schemaArray.push(prop);
        }
      }
    }
  }

  findMatchRoot(item: string) {
    let rname: string;
    item = item.toLowerCase();
    for (const r of this.rootLinks) {
      rname = r.value.toLowerCase();

      if (rname.match(item)) {
        if (rname.length - item.length < 2) {
          return r.value;
        }
      }
    }
  }

  deleteItem(link: string) {
    this.RepositoryService.removeItem(link).subscribe(
      data => console.log('Delete item' + JSON.stringify(data)),
      error => this.handleError(error),
    );
  }

  onEditItem(row) {
    this.getSchema();
    this.activeEdit = row;
    this.isNewItem = false;
    this.displayDialog = true;
  }

  saveToServer(row) {
    alert('data saved');
    if (row) {
      this.RepositoryService.saveItem(row.link._self, row);
    } else {
      this.RepositoryService.createItem(this.selfUrl, this.editAble);
    }
  }


  showDialogToAdd() {
    this.getSchema();
    this.isNewItem = true;
    this.activeEdit = {};
    this.displayDialog = true;
  }


  save() {
    const rows = [...this.rows];
    if (this.isNewItem) {
      this.rows.push(this.activeEdit);
      this.RepositoryService.createItem(this.activeTable, this.activeEdit).subscribe(
        data => this.getAllItems({value: this.activeTable}),
        error => this.handleError(error),
      );

    } else {
      this.rows[this.findSelectedItemIndex()] = this.activeEdit;
      this.RepositoryService.saveItem(this.activeEdit._links.self.href, this.activeEdit).subscribe(
        data => this.getAllItems({value: this.activeTable}),
        error => this.handleError(error),
      );

    }
    this.rows = rows;
    this.displayDialog = false;
    this.activeEdit = null;

  }

  onDelete() {
    const index = this.findSelectedItemIndex();
    this.rows = this.rows.filter((val, i) => i != index);
    this.activeEdit = null;
    this.displayDialog = false;
  }

  onRowSelect(event) {
    this.isNewItem = false;
    this.activeEdit = this.cloneTag(event.data);
    this.displayDialog = true;
  }

  cloneTag(t: Tag) {
    const tag = new Tag();
    for (const prop in t) {
      if (t.hasOwnProperty(prop)) {
        tag[prop] = t[prop];
      }
    }
    return tag;
  }

  findSelectedItemIndex() {
    return this.rows.indexOf(this.selectedItem);
  }

  searchPersons(event) {
    this.RepositoryService.searchPersons(event.query).subscribe(
      data => this.persons = data
    );
  }

  searchByName(item, event) {
    this.RepositoryService.searchByName(item, event.query).subscribe(
      data => this.searchResults[item] = data._embedded[item]
    );
  }

  handleError(error) {
    console.error(error);
  }
}


