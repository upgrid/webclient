import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DatatableComponent} from './datatable/datatable.component';
import {LoginComponent} from './login/login.component';

const routes: Routes = [
  // {path: '', redirectTo: '', pathMatch: 'full'},
  {path: '', component: DatatableComponent},
  {path: 'table/:section', component: DatatableComponent},
  {path: 'table/', component: DatatableComponent},
  {path: 'login', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
