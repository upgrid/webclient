import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {environment} from '../../environments/environment';

@Injectable()
export class LoginService {

  baseURL: string = 'http://' + window.location.hostname + ':8080/repo';


  constructor(private _http: Http) {
  }

  getToken(bota: string) {
    const headers: Headers = new Headers();
    headers.append('Authorization', 'Basic ' + bota);
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('X-Requested-With', 'XMLHttpRequest');
    return this._http.get(this.baseURL, {
      headers: headers
      }).map(res => res.json());
  }

}
