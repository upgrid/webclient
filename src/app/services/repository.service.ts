import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {environment} from '../../environments/environment';
import {AppComponent} from '../app.component';


@Injectable()
export class RepositoryService {


  baseURL: string = 'http://' + window.location.hostname + ':8080/repo';
  headers: Headers = new Headers();
  schemaHeaders: Headers = new Headers();

  static remapLinks(links: any) {
    const r = [];
    if (links) {
      for (const link in links) {
        if (links.hasOwnProperty(link)) {
          r.push({
            name: link,
            href: links[link].href,
            label: link
          });
        }
      }
    }
    return r;
  }

  static remapArray(resources: any) {
    const result = {
      data: [],
      page: {},
      links: []
    };
    if (!resources.hasOwnProperty('_embedded')) {
      resources._linksArray = RepositoryService.remapLinks(resources._links);
      result.data.push(resources);
    } else {
      resources = resources._embedded;
      for (const resource in resources) {
        if (resources.hasOwnProperty(resource)) {
          for (const item of resources[resource]) {
            item._linksArray = RepositoryService.remapLinks(item._links);
            result.data.push(item);
          }
        }

      }
    }


    console.log(result);
    return result;
  }

  // private static handleError(error: Response) {
  //   console.error(error);
  //   return Observable.throw(error || 'Server error');
  // }

  constructor(private http: Http) {
    this.headers.append('content-type', 'application/json; charset=UTF-8');

    this.schemaHeaders.append('Accept', 'application/schema+json');
    // this.schemaHeaders.append('Authorization', 'Basic ' + AppComponent.userToken);
    // this.headers.append('Authorization', 'Basic ' + AppComponent.userToken);
  }

  getUrl(url: string) {
    return this.http.get(url,
      {
        headers: this.headers
      }).map(res => RepositoryService.remapArray(res.json()));
  }


  getItem(item: string, itemId: any = '') {
    // this.checkIfTokenAdded();
    return this.getUrl(this.baseURL + '/' + item.toString() + '/' + itemId.toString());

  }

  getSchema(item: string) {
    return this.http.get(this.baseURL + '/profile/' + item,
      {
        headers: this.schemaHeaders
      }).map(res => res.json());
  }

  removeItem(url: string) {
    return this.http.delete(url,
      {
        headers: this.headers
      }).map(res => console.log('Item deleted; response: ' + res.json()));
  }

  saveItem(url: string, body: any) {
    body = this.checkRemap(body);
    return this.http.put(url, body,
      {
        headers: this.headers
      });

  }

  createItem(item: string, body: any) {
    body = this.checkRemap(body);
    return this.http.post(this.baseURL + '/' + item, body,
      {
        headers: this.headers
      });
  }

  checkRemap(body: any) {
    body = this.cloneItem(body);
    for (const item of ['personGroups', 'zones', 'persons', 'tags', 'idCards', 'doors', 'person']) {
      if (body.hasOwnProperty(item)) {
        body[item] = this.remapAnswerArray(body[item]);
      }
    }

    return body;
  }

  cloneItem(b: any) {
    const body = {};
    for (const prop in b) {
      if (b.hasOwnProperty(prop)) {
        body[prop] = b[prop];
      }
    }
    return body;
  }

  searchPersons(name: string) {
    return this.http.get(
      this.baseURL + '/persons/search/searchByFirstLaneOrLastName?name=' + name + '&projection=olnyName',
      {
        headers: this.headers
      }
    ).map(res => res.json());
  }

  searchByName(item: string, name: string) {
    return this.http.get(this.baseURL + '/' + item + '/search/names?name=' + name + '&projection=olnyName',
      {
        headers: this.headers
      }
    ).map(res => res.json());
  }

  remapAnswerArray(input) {
    let result = [];
    if (!input.hasOwnProperty('_links')) {
      for (const r of input) {
        result.push(r._links.self.href);
      }
    } else {
      result = input._links.self.href;
    }
    return result;
  }
}
