///<reference path="../../../node_modules/@angular/http/src/http.d.ts"/>
import {Injectable} from '@angular/core';
import {Http, RequestOptionsArgs} from '@angular/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class Hypermedia {
  private headers: any;

  constructor(private http: Http) {
    this.headers.append('content-type', 'application/json; charset=UTF-8');
    // this.headers.append('X-token', AppComponent.userToken);
  }

  get(links: any[], rel: String, url: string, options?: RequestOptionsArgs) {
    let link = null;
    const request = null;

    // Find the right link
    links.forEach(function (_link) {
      if (_link.rel === rel) {
        link = _link;
        return;
      }
    });

    return this.http.get(link.href, {
      headers: this.headers
    });
  }
}
