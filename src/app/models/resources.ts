export class DoorLog {
  id: number;
  person: Person;
  tag: Tag;
  idcard: IdCard;
  level: number;
  message: string;
  door: Door;
  _links: any;
}

export class Person {
  id: number;
  firstName: string;
  lastName: string;
  idCode: number;
  active: boolean;
  personGroups: PersonGroup[];
  tags: Tag[];
  idCards: IdCard[];
  doorLog: DoorLog[];
  _links: any;
}

export class Tag {
  id: number;
  uid: string;
  name: string;
  active: boolean;
  person: Person;
  _links: any;
}

export class IdCard {
  id: number;
  document: string;
  active: boolean;
  person: Person;
  doorLog: DoorLog[];
  _links: any;
}

export class Door {
  id: number;
  name: string;
  zone: Zone[];
  doorLog: DoorLog[];
  _links: any;
}


export class PersonGroup {
  id: number;
  name: string;
  zones: Zone[];
  persons: Person[];
  _links: any;
}

export class Zone {
  id: number;
  name: string;
  type: string;
  personGroups: PersonGroup[];
  doors: Door[];
  _links: any;
}
